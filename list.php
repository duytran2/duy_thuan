<!DOCTYPE html>
<html lang="en">
<?php
require_once('./assets/views/partials/head.php');
?>

<!-- HEADER -->
<div class="header">
    <div class="container g-0 h-100">
        <div class="header__wrap">
            <div class="header__logo">
                <a href="#" class="link--format header__logo-link">
                    <img class="header__logo-img" src="./assets/images/main_logo.svg" alt="" />
                </a>
            </div>

            <div class="header__info w-100">
                <div class="header__brand-name">
                    WVVF - World Vovinam Federation
                </div>
                <div class="header__navbar">
                    <div class="navbar__mini" id="btnMenu">
                        <span class="navbar__mini__item-icon">
                            <i class="las la-bars"></i>
                        </span>
                        <span class="navbar__mini__title">menu</span>
                    </div>
                    <ul id="navbarList" class="navbar__list list-unstyled mb-0">
                        <li class="navbar__item">
                            <div class="navbar__item__wrap w-100">
                                <a href="#" class="link--format navbar__item-link">
                                    About WVVF
                                </a>
                                <span class="navbar__item-icon">
                                    <i class="las la-angle-right navbar__item-icon--right"></i>
                                    <i class="las la-angle-down navbar__item-icon--down"></i>
                                </span>
                            </div>
                            <ul class="subnav list-unstyled">
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu</a>
                                </li>
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu 2</a>
                                </li>
                            </ul>
                        </li>
                        <li class="navbar__item">
                            <div class="navbar__item__wrap w-100">
                                <a href="#" class="link--format navbar__item-link">
                                    World Council of Masters
                                </a>
                                <span class="navbar__item-icon">
                                    <i class="las la-angle-right navbar__item-icon--right"></i>
                                    <i class="las la-angle-down navbar__item-icon--down"></i>
                                </span>
                            </div>
                            <ul class="subnav list-unstyled">
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu</a>
                                </li>
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu 2</a>
                                </li>
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu 3</a>
                                </li>
                            </ul>
                        </li>
                        <li class="navbar__item">
                            <div class="navbar__item__wrap w-100">
                                <a href="#" class="link--format navbar__item-link">
                                    Vovinam Viet Vo Dao
                                </a>
                                <span class="navbar__item-icon">
                                    <i class="las la-angle-right navbar__item-icon--right"></i>
                                    <i class="las la-angle-down navbar__item-icon--down"></i>
                                </span>
                            </div>
                            <ul class="subnav list-unstyled">
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu</a>
                                </li>
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu 2</a>
                                </li>
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu 3</a>
                                </li>
                            </ul>
                        </li>
                        <li class="navbar__item">
                            <div class="navbar__item__wrap w-100">
                                <a href="#" class="link--format navbar__item-link">
                                    News
                                </a>
                                <span class="navbar__item-icon">
                                    <i class="las la-angle-right navbar__item-icon--right"></i>
                                    <i class="las la-angle-down navbar__item-icon--down"></i>
                                </span>
                            </div>
                            <ul class="subnav list-unstyled">
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu</a>
                                </li>
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu 2</a>
                                </li>
                            </ul>
                        </li>
                        <li class="navbar__item">
                            <div class="navbar__item__wrap w-100">
                                <a href="#" class="link--format navbar__item-link">
                                    Events
                                </a>
                                <span class="navbar__item-icon">
                                    <i class="las la-angle-right navbar__item-icon--right"></i>
                                    <i class="las la-angle-down navbar__item-icon--down"></i>
                                </span>
                            </div>
                            <ul class="subnav list-unstyled">
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu</a>
                                </li>
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu 2</a>
                                </li>
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu 3</a>
                                </li>
                            </ul>
                        </li>
                        <li class="navbar__item">
                            <div class="navbar__item__wrap w-100">
                                <a href="#" class="link--format navbar__item-link">
                                    Gallery
                                </a>
                                <span class="navbar__item-icon">
                                    <i class="las la-angle-right navbar__item-icon--right"></i>
                                    <i class="las la-angle-down navbar__item-icon--down"></i>
                                </span>
                            </div>
                            <ul class="subnav list-unstyled">
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu</a>
                                </li>
                                <li class="subnav__item">
                                    <a href="#" class="link--format subnav__link">Item submenu 2</a>
                                </li>
                            </ul>
                        </li>
                        <li class="navbar__item">
                            <div class="navbar__item__wrap w-100">
                                <a href="#" class="link--format navbar__item-link">
                                    Documents
                                </a>
                            </div>
                        </li>
                        <li class="navbar__item">
                            <div class="navbar__item__wrap w-100">
                                <a href="#" class="link--format navbar__item-link">
                                    Contact
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- HEADER -->

<!-- MAIN CONTENT -->
<div class="main">
    <!-- MAIN CONTENT WRAP -->
    <div class="container g-0">
        <div class="content">
            <div class="row">
                <!-- LEFT -->
                <!-- <div class="content__left col-sm-12 col-md col-lg-3">
                    <div class="content__panel">
                        <div class="content__panel-wrap">
                            <h3 class="content__title">Follow us on</h3>
                            <ul class="connect__list list-unstyled my-0">
                                <li class="connect__list-item">
                                    <div class="connect__logo">
                                        <a href="" class="connect__logo-link">
                                            <img class="connect__logo-img" src="./assets/images/logo_facebook.png" alt="" />
                                        </a>
                                    </div>
                                </li>
                                <li class="connect__list-item">
                                    <div class="connect__logo">
                                        <a href="" class="connect__logo-link">
                                            <img class="connect__logo-img" src="./assets/images/logo_youtube.png" alt="" />
                                        </a>
                                    </div>
                                </li>
                                <li class="connect__list-item">
                                    <div class="connect__logo">
                                        <a href="" class="connect__logo-link">
                                            <img class="connect__logo-img" src="./assets/images/logo_insta.png" alt="" />
                                        </a>
                                    </div>
                                </li>
                                <li class="connect__list-item">
                                    <div class="connect__logo">
                                        <a href="" class="connect__logo-link">
                                            <img class="connect__logo-img" src="./assets/images/logo_twitter.png" alt="" />
                                        </a>
                                    </div>
                                </li>
                                <li class="connect__list-item">
                                    <div class="connect__logo">
                                        <a href="" class="connect__logo-link">
                                            <img class="connect__logo-img" src="./assets/images/logo_linkedln.png" alt="" />
                                        </a>
                                    </div>
                                </li>
                                <li class="connect__list-item">
                                    <div class="connect__logo">
                                        <a href="" class="connect__logo-link">
                                            <img class="connect__logo-img" src="./assets/images/logo_line.png" alt="" />
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="content__panel content__panel--background">
                        <div class="content__panel-wrap">
                            <h3 class="content__title">
                                VOVINAM PHILOSOPHY
                            </h3>
                            <p class="content__left-description">
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Nisi, blandit et
                                quisque sit. Sit ut amet dapibus dui
                                leo. Posuere quisque vel urna cras porta
                                sapien nec urna urna. Eu ultricies et
                                tortor a porttitor arcu eget ut.
                            </p>

                            <div class="row g-0">
                                <div class="col">
                                    <p class="content__left-description">
                                        Lorem ipsum dolor sit amet,
                                        consectetur adipiscing elit.
                                        Nisi, but.
                                    </p>
                                </div>
                                <div class="col content__left-background__wrap">
                                    <div class="content__left-background"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- LEFT -->

                <!-- CENTER -->
                <div class="col-sm-12 col-md-12 col-lg-9">

                    <div class="content__center">
                        <!-- SWIPER -->
                        <!-- <div class="swiper">
                            <div class="swiper-wrapper">
                                <div class="slider__wrap swiper-slide">
                                    <img src="./assets/images/slider_1.jpg" alt="" class="slider__img" />
                                </div>
                                <div class="slider__wrap swiper-slide">
                                    <img src="./assets/images/slider_2.jpg" alt="" class="slider__img" />
                                </div>
                                <div class="slider__wrap swiper-slide">
                                    <img src="./assets/images/slider_3.jpg" alt="" class="slider__img" />
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div> -->
                        <!-- SWIPER -->

                        <!-- CENTER CONTENT -->
                        <div class="content__panel">
                            <div class="center__panel-wrap">
                                <div class="center__header">
                                    <div class="center__header-tag"></div>
                                    <h1 class="center__header-title">
                                        latest news
                                    </h1>
                                </div>

                                <div id="mainContentCenter" class="row">
                                    <div class="col-12">
                                        <div class="center__item active">
                                            <a href="#" class="center__item--link link--format">
                                                <div class="center__wrap">
                                                    <div class="center__tag">
                                                        Sport
                                                    </div>
                                                    <div class="center__wrap-img">
                                                        <img src="./assets/images/slider_1.jpg" alt="" class="center__img" />
                                                    </div>
                                                    <h3 class="center__title">
                                                        Score Ippon to the
                                                        Coronavirus
                                                    </h3>
                                                    <div class="center__info">
                                                        <p class="center__time">
                                                            12 Aug 2020
                                                        </p>
                                                        <p class="center__author">
                                                            Tran Khanh Duy
                                                        </p>
                                                    </div>
                                                    <p class="center__description">
                                                        Lorem ipsum dolor
                                                        sit amet,
                                                        consectetur
                                                        adipiscing elit. Sit
                                                        id aliquet semper
                                                        libero sit id.
                                                        Tortor arcu mauris
                                                        neque quam ut id ut.
                                                        Erat porttitor
                                                        nullam sed purus id
                                                        senectus dictum. Eu
                                                        sit pellentesque
                                                        amet lectus. Lorem
                                                        ipsum dolor sit
                                                        amet, consectetur
                                                        adipiscing elit....
                                                    </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="center__item">
                                            <a href="#" class="center__item--link link--format">
                                                <div class="center__wrap">
                                                    <div class="center__tag">
                                                        Sport
                                                    </div>
                                                    <div class="center__wrap-img">
                                                        <img src="./assets/images/slider_2.jpg" alt="" class="center__img" />
                                                    </div>
                                                    <h3 class="center__title">
                                                        Score Ippon to the
                                                        Coronavirus
                                                    </h3>
                                                    <div class="center__info">
                                                        <p class="center__time">
                                                            12 Aug 2020
                                                        </p>
                                                        <p class="center__author">
                                                            Tran Khanh Duy
                                                        </p>
                                                    </div>
                                                    <p class="center__description">
                                                        Lorem ipsum dolor
                                                        sit amet,
                                                        consectetur
                                                        adipiscing elit. Sit
                                                        id aliquet semper
                                                        libero sit id.
                                                        Tortor arcu mauris
                                                        neque quam ut id ut.
                                                        Erat porttitor
                                                        nullam sed purus id
                                                        senectus dictum. Eu
                                                        sit pellentesque
                                                        amet lectus. Lorem
                                                        ipsum dolor sit
                                                        amet, consectetur
                                                        adipiscing elit....
                                                    </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="center__item">
                                            <a href="#" class="center__item--link link--format">
                                                <div class="center__wrap">
                                                    <div class="center__tag">
                                                        Sport
                                                    </div>
                                                    <div class="center__wrap-img">
                                                        <img src="./assets/images/slider_3.jpg" alt="" class="center__img" />
                                                    </div>
                                                    <h3 class="center__title">
                                                        Special Charity
                                                        Auction by
                                                        TestTestTestTest4z
                                                    </h3>
                                                    <div class="center__info">
                                                        <p class="center__time">
                                                            12 Aug 2020
                                                        </p>
                                                        <p class="center__author">
                                                            Tran Khanh Duy
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="center__item">
                                            <a href="#" class="center__item--link link--format">
                                                <div class="center__wrap">
                                                    <div class="center__tag">
                                                        Sport
                                                    </div>
                                                    <div class="center__wrap-img">
                                                        <img src="./assets/images/slider_4.jpg" alt="" class="center__img" />
                                                    </div>
                                                    <h3 class="center__title">
                                                        Score Ippon to the
                                                        Coronavirus
                                                    </h3>
                                                    <div class="center__info">
                                                        <p class="center__time">
                                                            12 Aug 2020
                                                        </p>
                                                        <p class="center__author">
                                                            Tran Khanh Duy
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="center__item">
                                            <a href="#" class="center__item--link link--format">
                                                <div class="center__wrap">
                                                    <div class="center__tag">
                                                        Sport
                                                    </div>
                                                    <div class="center__wrap-img">
                                                        <img src="./assets/images/slider_5.jpg" alt="" class="center__img" />
                                                    </div>
                                                    <h3 class="center__title">
                                                        Score Ippon to the
                                                        Coronavirus
                                                    </h3>
                                                    <div class="center__info">
                                                        <p class="center__time">
                                                            12 Aug 2020
                                                        </p>
                                                        <p class="center__author">
                                                            Tran Khanh Duy
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div id="loadMore" class="more-btn__wrap">
                                    <a type="button" href="" class="link--format more-btn">
                                        <span class="more-btn__text">
                                            see more news
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- CENTER CONTENT -->
                    </div>

                    <div class="content__center">
                        <!-- SWIPER -->
                        <!-- <div class="swiper">
                            <div class="swiper-wrapper">
                                <div class="slider__wrap swiper-slide">
                                    <img src="./assets/images/slider_1.jpg" alt="" class="slider__img" />
                                </div>
                                <div class="slider__wrap swiper-slide">
                                    <img src="./assets/images/slider_2.jpg" alt="" class="slider__img" />
                                </div>
                                <div class="slider__wrap swiper-slide">
                                    <img src="./assets/images/slider_3.jpg" alt="" class="slider__img" />
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div> -->
                        <!-- SWIPER -->

                        <!-- CENTER CONTENT -->
                        <div class="content__panel">
                            <div class="center__panel-wrap">
                                <div class="center__header">
                                    <div class="center__header-tag"></div>
                                    <h1 class="center__header-title">
                                        latest news
                                    </h1>
                                </div>

                                <div id="mainContentCenter" class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                        <div class="center__item">
                                            <a href="#" class="center__item--link link--format">
                                                <div class="center__wrap">
                                                    <div class="center__tag">
                                                        Sport
                                                    </div>
                                                    <div class="center__wrap-img">
                                                        <img src="./assets/images/slider_2.jpg" alt="" class="center__img" />
                                                    </div>
                                                    <h3 class="center__title">
                                                        Score Ippon to the
                                                        Coronavirus
                                                    </h3>
                                                    <div class="center__info">
                                                        <p class="center__time">
                                                            12 Aug 2020
                                                        </p>
                                                        <p class="center__author">
                                                            Tran Khanh Duy
                                                        </p>
                                                    </div>
                                                    <p class="center__description">
                                                        Lorem ipsum dolor
                                                        sit amet,
                                                        consectetur
                                                        adipiscing elit. Sit
                                                        id aliquet semper
                                                        libero sit id.
                                                        Tortor arcu mauris
                                                        neque quam ut id ut.
                                                        Erat porttitor
                                                        nullam sed purus id
                                                        senectus dictum. Eu
                                                        sit pellentesque
                                                        amet lectus. Lorem
                                                        ipsum dolor sit
                                                        amet, consectetur
                                                        adipiscing elit....
                                                    </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                        <div class="center__item">
                                            <a href="#" class="center__item--link link--format">
                                                <div class="center__wrap">
                                                    <div class="center__tag">
                                                        Sport
                                                    </div>
                                                    <div class="center__wrap-img">
                                                        <img src="./assets/images/slider_3.jpg" alt="" class="center__img" />
                                                    </div>
                                                    <h3 class="center__title">
                                                        Special Charity
                                                        Auction by
                                                        TestTestTestTest4z
                                                    </h3>
                                                    <div class="center__info">
                                                        <p class="center__time">
                                                            12 Aug 2020
                                                        </p>
                                                        <p class="center__author">
                                                            Tran Khanh Duy
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                        <div class="center__item">
                                            <a href="#" class="center__item--link link--format">
                                                <div class="center__wrap">
                                                    <div class="center__tag">
                                                        Sport
                                                    </div>
                                                    <div class="center__wrap-img">
                                                        <img src="./assets/images/slider_4.jpg" alt="" class="center__img" />
                                                    </div>
                                                    <h3 class="center__title">
                                                        Score Ippon to the
                                                        Coronavirus
                                                    </h3>
                                                    <div class="center__info">
                                                        <p class="center__time">
                                                            12 Aug 2020
                                                        </p>
                                                        <p class="center__author">
                                                            Tran Khanh Duy
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                        <div class="center__item">
                                            <a href="#" class="center__item--link link--format">
                                                <div class="center__wrap">
                                                    <div class="center__tag">
                                                        Sport
                                                    </div>
                                                    <div class="center__wrap-img">
                                                        <img src="./assets/images/slider_5.jpg" alt="" class="center__img" />
                                                    </div>
                                                    <h3 class="center__title">
                                                        Score Ippon to the
                                                        Coronavirus
                                                    </h3>
                                                    <div class="center__info">
                                                        <p class="center__time">
                                                            12 Aug 2020
                                                        </p>
                                                        <p class="center__author">
                                                            Tran Khanh Duy
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div id="loadMore" class="more-btn__wrap">
                                    <a type="button" href="" class="link--format more-btn">
                                        <span class="more-btn__text">
                                            see more news
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- CENTER CONTENT -->
                    </div>
                </div>

                <!-- CENTER -->

                <!-- RIGHT -->
                <div class="content__right col-sm-12 col-md col-lg-3">
                    <div class="content__panel">
                        <div class="content__panel-wrap">
                            <h3 class="content__title">
                                MASTERS AND TEACHERS
                            </h3>
                            <div class="right__list-wrap">
                                <ul class="right__list list-unstyled">
                                    <li class="right__list-item">
                                        <a href="#" class="right__list-item__link link--format">
                                            <div class="item__wrap">
                                                <div class="right__img__wrap">
                                                    <img src="./assets/images/avt_1.jpg" alt="" class="item__img" />
                                                </div>

                                                <div class="item__info">
                                                    <span class="item__name">
                                                        Annette Black
                                                    </span>
                                                    <span class="item__description">
                                                        Lorem ipsum
                                                        dolor sit amet.
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="right__list-item">
                                        <a href="#" class="right__list-item__link link--format">
                                            <div class="item__wrap">
                                                <div class="right__img__wrap">
                                                    <img src="./assets/images/avt_2.jpg" alt="" class="item__img" />
                                                </div>

                                                <div class="item__info">
                                                    <span class="item__name">
                                                        Jacob Jones
                                                    </span>
                                                    <span class="item__description">
                                                        Lorem ipsum
                                                        dolor sit amet.
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="right__list-item">
                                        <a href="#" class="right__list-item__link link--format">
                                            <div class="item__wrap">
                                                <div class="right__img__wrap">
                                                    <img src="./assets/images/avt_3.jpg" alt="" class="item__img" />
                                                </div>

                                                <div class="item__info">
                                                    <span class="item__name">
                                                        Dianne Russell
                                                    </span>
                                                    <span class="item__description">
                                                        Lorem ipsum
                                                        dolor sit amet.
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="right__list-item">
                                        <a href="#" class="right__list-item__link link--format">
                                            <div class="item__wrap">
                                                <div class="right__img__wrap">
                                                    <img src="./assets/images/avt_4.jpg" alt="" class="item__img" />
                                                </div>

                                                <div class="item__info">
                                                    <span class="item__name">
                                                        Bessie Cooper
                                                    </span>
                                                    <span class="item__description">
                                                        Lorem ipsum
                                                        dolor sit amet.
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="right__list-item">
                                        <a href="#" class="right__list-item__link link--format">
                                            <div class="item__wrap">
                                                <div class="right__img__wrap">
                                                    <img src="./assets/images/avt_5.jpg" alt="" class="item__img" />
                                                </div>

                                                <div class="item__info">
                                                    <span class="item__name">
                                                        Albert Flores
                                                    </span>
                                                    <span class="item__description">
                                                        Lorem ipsum
                                                        dolor sit amet.
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="content__panel">
                        <div class="content__panel-wrap">
                            <h3 class="content__title">
                                Member Federation
                            </h3>
                            <div class="right__list-wrap">
                                <ul class="right__list list-unstyled">
                                    <li class="member-list__item">
                                        <a href="#" class="member-item__link link--format">
                                            Asia
                                        </a>
                                    </li>
                                    <li class="member-list__item">
                                        <a href="#" class="member-item__link link--format">
                                            Africa
                                        </a>
                                    </li>
                                    <li class="member-list__item">
                                        <a href="#" class="member-item__link link--format">
                                            Australia
                                        </a>
                                    </li>
                                    <li class="member-list__item">
                                        <a href="#" class="member-item__link link--format">
                                            Europe
                                        </a>
                                    </li>
                                    <li class="member-list__item">
                                        <a href="#" class="member-item__link link--format">
                                            North America
                                        </a>
                                    </li>
                                    <li class="member-list__item">
                                        <a href="#" class="member-item__link link--format">
                                            South America
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- RIGHT -->
            </div>
        </div>
    </div>
    <!-- MAIN CONTENT WRAP -->
</div>
<!-- MAIN CONTENT -->

<!-- FOOTER -->
<?php
require_once('./assets/views/partials/footer.php');
?>
<!-- FOOTER -->

<!-- Swiper JS -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- Swiper JS -->

<!-- External JS -->
<script src="./assets/js/main.js"></script>
<!-- External JS -->
</body>

</html>