<div class="footer">
    <div class="footer__wrap">
        <div class="footer__body">
            <div class="footer__logo-wrap">
                <img src="./assets/images/main_logo.svg" alt="" class="footer__logo-img" />
            </div>
            <div class="footer__brand-name">
                WVVF - World Vovinam Federation
            </div>
            <p class="footer__copyright">
                Copyright 2012 - 2020. All rights reserved.
            </p>
            <ul class="footer__nav list-unstyled">
                <li class="footer__nav-item">
                    <a href="" class="footer__nav-link link--format">Legal Infomation</a>
                </li>
                <li class="footer__nav-item">
                    <a href="" class="footer__nav-link link--format">FAQ</a>
                </li>
                <li class="footer__nav-item">
                    <a href="" class="footer__nav-link link--format">Credits</a>
                </li>
                <li class="footer__nav-item">
                    <a href="" class="footer__nav-link link--format">Sitemap</a>
                </li>
            </ul>
        </div>
    </div>
</div>