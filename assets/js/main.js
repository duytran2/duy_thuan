// Swiper
const swiper = new Swiper('.swiper', {
    // Optional parameters
    loop: true,
    autoplay: true,
    autoplay: {
        delay: 2000,
        pauseOnMouseEnter: true,
        disableOnInteraction: false
    },
    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        // bulletClass: 'test-bullet',
        // bulletActiveClass: 'test-bullet--active',
        clickable: true,
        // renderBullet: function (index, bulletClass) {
        //     return '<span class="' + bulletClass + '">' + (index + 1) + '</span>';
        // },
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});


$(document).ready(function () {
    var navBar = $('#navbarList')
    var mainContentCenter = $('#mainContentCenter')
    var loadMoreBtn = $('#loadMore')

    // Toggle menu
    $('#btnMenu').on('click', function () {
        navBar.toggleClass('active')
    })

    // Load more news
    loadMoreBtn.on('click', function (e) {
        $('.more-btn').addClass('more-btn--loading');

        setTimeout(function () {
            for (let i = 0; i < 4; i++) {
                mainContentCenter.append('<div class="col-lg-6 col-md-6 col-sm-6 col-12">' +
                    '<div class="center__item">' +
                    '<a href="#" class="center__item--link link--format">' +
                    '<div class="center__wrap">' +
                    '<div class="center__tag">Sport</div>' +
                    '<div class="center__wrap-img">' +
                    '<img src="./assets/images/slider_' + (i + 1) + '.jpg" alt="" class="center__img"/>' +
                    '</div>' +
                    '<h3 class="center__title"> Score Ippon to the Coronavirus</h3>' +
                    '<div class="center__info" >' +
                    '<p class="center__time">12 Aug 2020</p>' +
                    '<p class="center__author" > Tran Khanh Duy </p>' +
                    '</div> <p class="center__description">' +
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit id aliquet semper libero sit id. Tortor arcu mauris neque quam ut id ut. Erat porttitor nullam sed purus id senectus dictum. Eu sit pellentesque amet lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit....' +
                    '</p></div> </a> </div> </div>')
            }

            $('.more-btn').removeClass('more-btn--loading');
        }, 2000);

        e.preventDefault()
    })

    // Toggle submenu when mobile/tablet devide
    $('.navbar__item').on('click', function () {
        $(".subnav").not($(this).children()).hide()
        $(this).children(".subnav").toggle()
    })

    // Click outside hide submenu
    $(this).on('click', function (e) {
        if (!$(e.target).closest(".navbar__item").length && !$(e.target).closest("#btnMenu").length) {
            $(".subnav").hide()
            navBar.removeClass('active')
        }
    })

})